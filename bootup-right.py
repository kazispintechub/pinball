import cv2 as cv
import numpy as np
import os

RIGHTWINDOW = 'Right'
current_path = os.getcwd()
path = current_path + r'/media/bootup'
right_paths = [
     f.path for f in os.scandir(path) if f.is_dir() and 'right' in f.name
]

# print(right_paths)


def rescaleFrame(frame, scale_width=0.75, scale_height=1.335):
    width = int(frame.shape[1] * scale_width)
    height = int(frame.shape[0] * scale_height)
    dimensions = (width, height)
    return cv.resize(frame, dimensions, interpolation=cv.INTER_AREA)


for path in right_paths:
    video_file_path = path + '/' + os.walk(path).__next__()[2][0]
    capture = cv.VideoCapture(video_file_path)
    while(capture.isOpened()):
        isTrue, rightFrame = capture.read()
        if isTrue is True:
            cv.namedWindow(RIGHTWINDOW)
            cv.moveWindow(RIGHTWINDOW, 960, 0)
            right_resized_video_frame = rescaleFrame(rightFrame, 0.75, 0.75)
            cv.imshow(RIGHTWINDOW, right_resized_video_frame)
            if cv.waitKey(25) & 0xFF == ord('q'):
                break
        else:
            # blank = np.zeros((600, 960), dtype='uint8')
            blank = np.ones(shape=(550, 960, 3), dtype=np.int8)
            cv.putText(
                img=blank,
                text="Type y for YES",
                org=(150, 150),
                fontFace=cv.FONT_HERSHEY_SIMPLEX,
                fontScale=3,
                color=(0, 255, 0),
                thickness=3,
                lineType=cv.LINE_AA
            )
            cv.imshow(RIGHTWINDOW, blank)
            if cv.waitKey(0) & 0xFF == ord('y'):
                break
    capture.release()
cv.destroyAllWindows()
