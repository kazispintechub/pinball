import cv2 as cv
import os

LEFTWINDOW = 'Left'
current_path = os.getcwd()
path = current_path + r'/media/bootup'

left_paths = [
    f.path for f in os.scandir(path) if f.is_dir() and 'left' in f.name
]

# print(left_paths)


def rescaleFrame(frame, scale_width=0.75, scale_height=1.335):
    width = int(frame.shape[1] * scale_width)
    height = int(frame.shape[0] * scale_height)
    dimensions = (width, height)
    return cv.resize(frame, dimensions, interpolation=cv.INTER_AREA)


for path in left_paths:
    video_file_path = path + '/' + os.walk(path).__next__()[2][0]
    capture = cv.VideoCapture(video_file_path)
    while(capture.isOpened()):
        isTrue, rightFrame = capture.read()
        if isTrue is True:
            cv.namedWindow(LEFTWINDOW)
            cv.moveWindow(LEFTWINDOW, 0, 0)
            left_resized_video_frame = rescaleFrame(rightFrame, 0.75, 0.75)
            cv.imshow(LEFTWINDOW, left_resized_video_frame)
            if cv.waitKey(25) & 0xFF == ord('n'):
                break
        else:
            break
    capture.release()
cv.destroyAllWindows()
